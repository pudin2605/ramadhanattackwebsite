<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Auth;

class AdminController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function show(){
    	if(Auth::guest()){
    		return redirect()->action('Controller@index');
    	}
    	return view('admin');
    }

    public function showLogin(){
    	return view('admin-login');
    }

    public function processLogin(){
    	if (Auth::attempt(['email' => $email, 'password' => $password]))
        {
            return redirect()->action('AdminController@show');
        }
    }
}
